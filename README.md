# Count Up - Web Komponente

### [Jamie Perkins](https://github.com/inorganik) | [Doku](https://github.com/inorganik/CountUp.js) | [Demo](https://inorganik.github.io/countUp.js/)

<hr>

CountUp.js ist eine abhängigkeitsfreie, leichtgewichtige Javascript-Klasse, mit der sich schnell Animationen erstellen lassen, 
die numerische Daten auf interessantere Weise darstellen.

## Installation

### Npm
```bash
echo @a-team25:registry=https://gitlab.com/api/v4/projects/47251637/packages/npm/ >> .npmrc
```

```bash
npm i @a-team25/at-count-up
```

### Yarn
```bash
echo \"@a-team25:registry\" \"https://gitlab.com/api/v4/projects/47251637/packages/npm/\" >> .yarnrc
```

```bash
yarn add @a-team25/at-count-up
```

## Benutzung

### Import
Die Komponente muss nur in der "Haupt" JavaScript-Datei des Projekts importiert werden. <br>
Nach dem Import steht die Komponente **global** im Projekt zur Verfügung und es muss nichts weiter getan werden.

```javascript
import '@a-team25/at-count-up'
```

### Attribute

| Name                     | Default | Description                                             |
|:-------------------------|:--------|:--------------------------------------------------------|
| `start-val`              | `0`     | number to start at                                      |
| `decimal-places`         | `0`     | number of decimal places                                |
| `duration`               | `4`     | animation duration in seconds                           |
| `use-grouping`           | `true`  | example: 1,000 vs 1000                                  |
| `use-indian-separators`  | `false` | example: 1,00,000 vs 100,000                            |
| `use-easing`             | `true`  | ease animation                                          |
| `smart-easing-threshold` | `999`   | smooth easing for large numbers above this if useEasing |
| `smart-easing-amount`    | `333`   | amount to be eased for numbers above threshold)         |
| `separator`              | `'.'`   | grouping separator (',')                                |
| `decimal`                | `','`   | decimal ('.'))                                          |
| `prefix`                 | `''`    | text prepended to result                                |
| `suffix`                 | `''`    | text appended to result                                 |
| `numerals`               | `[]`    | numeral glyph substitution                              |
| `enable-scroll-spy`      | `true`  | start animation when target is in view                  |
| `scroll-spy-delay`       | `0`     | delay (ms) after target comes into view                 |
| `scroll-spy-once`        | `false` |                                                         |

### Template

```html
<at-count-up start-val="50" duration="10" prefix="Preis: " suffix=" €">
    1000
</at-count-up>
```
