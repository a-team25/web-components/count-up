import {LitElement} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {CountUp, CountUpOptions} from "countup.js";

@customElement('adt-count-up')
export class AdtCountUp extends LitElement {

    @property()
    startVal?: number = 0;

    @property()
    decimalPlaces?: number = 0;

    @property()
    duration?: number = 4;

    @property()
    useGrouping?: boolean = true;

    @property()
    useIndianSeparators?: boolean = false;

    @property()
    useEasing?: boolean = true;

    @property()
    smartEasingThreshold?: number = 999;

    @property()
    smartEasingAmount?: number = 333;

    @property()
    separator?: string = '.';

    @property()
    decimal?: string = ',';

    @property()
    praefix?: string = '';

    @property()
    suffix?: string = '';

    @property()
    numerals?: string[] = [];

    @property()
    enableScrollSpy?: boolean = true;

    @property()
    scrollSpyDelay?: number = 0;

    @property()
    scrollSpyOnce?: boolean = false;

    constructor() {
        super();

        Array.from(this.attributes).forEach((attribute) => {
            const property = this._kebabToCamelCase(attribute.name);

            if (this[property] !== undefined) {
                switch (typeof this[property]) {
                    case 'number': this[property] = parseInt(attribute.value); break;
                    case 'boolean':this[property] = (attribute.value === 'true'); break;
                    case 'object': this[property] = JSON.parse(attribute.value); break;
                    default: this[property] = attribute.value;
                }
            }
        })
    }

    _kebabToCamelCase(str: string) {
        return str.replace(/-./g, x=>x[1].toUpperCase())
    }

    connectedCallback() {
        const endVal: number = parseInt(this.textContent ?? '100');

        new CountUp(this, endVal, this._getOptions());
    }

    _getOptions(): CountUpOptions {
        return {
            startVal: this.startVal,
            decimalPlaces: this.decimalPlaces,
            duration: this.duration,
            useGrouping: this.useEasing,
            useIndianSeparators: this.useIndianSeparators,
            useEasing: this.useEasing,
            smartEasingThreshold: this.smartEasingThreshold,
            smartEasingAmount: this.smartEasingAmount,
            separator: this.separator,
            decimal: this.decimal,
            prefix: this.praefix,
            suffix: this.suffix,
            numerals: this.numerals,
            enableScrollSpy: this.enableScrollSpy,
            scrollSpyDelay: this.scrollSpyDelay,
            scrollSpyOnce: this.scrollSpyOnce,
        }
    }
}
